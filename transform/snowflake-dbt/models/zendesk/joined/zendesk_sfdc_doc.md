{% docs zendesk_ticket_sfdc %}

This table contains data from the final Zendesk tickets table which includes all tickets regardless of satisfaction score as well as relevant customer metadata from Salesforce. 

This table allows for the reporter to generate analysis based on specific customer groups in SFDC. Additionally, this table provides each ticket with the correct customer segment from SFDC.

{% enddocs %}


